# FPU (Floating-Point Unit)

## 1. git repository organization

### adder, multiplier, common
The adder, multiplier and common directories contain the vhdl code for the operators.  
The operators' top-level entities are defined in adder/FloatAdder.vhdl and multiplier/FloatMultiplier.vhdl.

### testbench
The testbench directory contains testbench vhdl files, testbench data (test_vector.txt) and c++ code to generate testbench data.

### synthesis
The synthesis directory contains the scripts and configuration files needed to synthesize operators with _Synopsys Design Compiler_ and _Yosys_.

### simulation
The simulation directory contains modelsim scripts for gate-level simulation.

### simulation_output
The simulation_output directory contains simulation results for ghdl.

## Floating number format
The format is specified in common/FPUTypes.vhdl via the constants e and p.  
For IEEE-754 floats: e=8 p=24  
For 10-bit: e=4 p=6

## Rounding mode
For the moment, operators are in round-to-zero mode.  
It is also possible to select the round-to-nearest mode, but by commenting/uncommenting few lines... this should be improved.


## 2. Simulation

Depending on the floating-point type used to test operators, the format in common/FPUTypes.vhdl needs to be changed before synthesis/simulation.

### Simulation with ghdl
`make run-testbench-10b-multiplier`     //multiplication simulation for all 10-bit float combinations  
`make run-testbench-10b-adder`          //simulate addition for all 10-bit float combinations  
`make run-testbench-multiplier`         //multiplication simulation for random IEEE-754 32-bit float values  
`make run-testbench-adder`              //simulate addition for random IEEE-754 32-bit float values  

For random IEEE-754 32-bit float values, the number of random tests can be modified in testbench-adder.c and testbench-multiplier.c by changing the value of NBR_RANDOM_TESTS.  

If an error is detected during testbench, a warning containing the field (sign, exponent or mantissa) and the expected and found values appears in the terminal.

### Simulation with modelsim (RTL and gate-level)

Before running the simulation, generate the data used by the testbench by executing one of these commands:  
`make testbench-data-10b-multiplier`    //multiplication for all 10-bit float combinations  
`make testbench-data-10b-adder`         //addition for all 10-bit float combinations  
`make testbench-data-multiplier`          //multiplication for random IEEE-754 32-bit float values  
`make testbench-data-adder`               //addition for random IEEE-754 32-bit float values  
  
Then: 
```
cd simulation  
vsim&  
``` 
In modelsim's transcript window:  
```
do adder.do  
do multiply.do  
```

If an error is detected during the testbench, a warning containing the field (sign, exponent or mantissa) and the expected and found values appears in the modelsim transcript window.

## 3. Synthesis
```
cd synthesis  
dc_shell  
```
In dc_shell transcript window:
```
source adder.script  
source multiplier.script  
```

## 4. Licence

Copyright 2022 Inria and University of Rennes.
Copyright and related rights are licensed under the Solderpad Hardware
License, Version 2.1 (the "License"); you may not use this file except in
compliance with the License.  You may obtain a copy of the License at
http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
or agreed to in writing, software, hardware and materials distributed under
this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

###  Contributors:       
* Thomas Mevel - thomas.mevel@inria.fr                  
* Olivier Sentieys - olivier.sentieys@inria.fr           
* Silviu Filip - silviu.filip@inria.fr     
              
