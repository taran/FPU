-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    mbe_ppg                                                    --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    partial product encoder-generator                          --
-- receives 3 consecutive binary digits and calculates the product            --
-- of the resulting radix-4 mbe digit with the number a                       --
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mbe_ppg is
  generic(n : natural := 32);
  port(three_digits : in std_logic_vector(2 downto 0);--sign bit representation
       a : in std_logic_vector(N-1 downto 0);
       pp : out std_logic_vector(N downto 0);
       neg : out std_logic);
end mbe_ppg;

architecture arch of mbe_ppg is
  signal s_inc : std_logic;
begin
  with three_digits select pp <=
    (others => '0') when "000" | "111",
    '0'&a when "001" | "010",
    a&'0' when "011",
    --check this
    not ('0'&a) when "110" | "101",
    not (a&'0') when "100",
    (others => 'X') when others;
  
  with three_digits select neg <=
    '0' when "000" | "111" | "001" | "010" | "011",
    '1' when "110" | "101" | "100",
    'X' when others;
end architecture;
