-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    full_adder                                                 --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Full Adder                                                 --
--                                                                            --
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity full_adder is
  port(a   : in  std_logic;
       b   : in  std_logic;
       cin : in  std_logic;
       s   : out std_logic;
       cout : out std_logic);
end full_adder;

architecture behavior of full_adder is
  component half_adder
    port(a : in  std_logic;
         b : in  std_logic;
         s : out std_logic;
         cout : out std_logic);
  end component;
  
  signal s_1 : std_logic;
  signal c_1 : std_logic;
  signal c_2 : std_logic;
begin
  h0 : half_adder port map(a, b, s_1, c_1);
  h1 : half_adder port map(s_1, cin, s, c_2);
  cout <= c_1 or c_2;
end architecture;
