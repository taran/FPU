-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    half_adder                                                 --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Half Adder                                                 --
--                                                                            --
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity half_adder is
  port(a : in  std_logic;
       b : in  std_logic;
       s : out std_logic;
       cout : out std_logic);
end half_adder;

architecture arch of half_adder is
begin
  s <= a xor b;
  cout <= a and b;
end architecture;
