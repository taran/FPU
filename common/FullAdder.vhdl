-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    FullAdder                                                  --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Full Adder                                                 --
--                                                                            --
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
entity FullAdder is
port (a, c_in, b: in std_logic;
      sum, carry: out std_logic);
end entity FullAdder;

architecture structural of FullAdder is

  component HalfAdder is
    port (a, b: in std_logic; 
          sum, carry: out std_logic);
  end component HalfAdder;

  signal s1, s2, s3: std_logic;

begin

  H1: HalfAdder port map (a=>a, b=>b, sum=>s1, carry=>s3);

  H2: HalfAdder port map (a=>s1, b=>c_in, sum=>sum, carry=>s2);

carry<=s2 or s3;

end structural;

