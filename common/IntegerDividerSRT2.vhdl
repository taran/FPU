-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    IntegerDividerSRT2                                         --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Integer Divider  (srt division radix 2)                    --
--                                                                            --
--------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;

ENTITY IntegerDividerSRT2 IS
  GENERIC (n : POSITIVE := 1);
  PORT (
    num, den : IN STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
    q : OUT STD_LOGIC_VECTOR(n - 1 DOWNTO 0));
END ENTITY IntegerDividerSRT2;


ARCHITECTURE srt2 OF IntegerDividerSRT2 IS --srt division radix 2 using quotient digit set {-1,0,1}, den > num

BEGIN
  divsrt2 : PROCESS (num, den) IS
    VARIABLE vqm : STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
    VARIABLE vqp : STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
    VARIABLE vq : STD_LOGIC_VECTOR(n - 1 DOWNTO 0);

    VARIABLE vw : STD_LOGIC_VECTOR(n + 1 DOWNTO 0);
    VARIABLE vwtemp : STD_LOGIC_VECTOR(n + 1 DOWNTO 0);

  BEGIN
    vw(n + 1 DOWNTO n) := (OTHERS => '0');
    vw(n - 1 DOWNTO 0) := num;
    vqm := (OTHERS => '0');
    vqp := (OTHERS => '0');

    FOR i IN n - 1 DOWNTO 0 LOOP
      vw := STD_LOGIC_VECTOR(shift_left(unsigned(vw), 1));

      IF vw(n + 1) = '0' THEN
        vwtemp := STD_LOGIC_VECTOR(unsigned(vw) - unsigned(den));
        IF vwtemp(n + 1) = '0' THEN
          vw := vwtemp;
          vqp(i) := '1';
        END IF;

      ELSE

        vwtemp := STD_LOGIC_VECTOR(unsigned(vw) + unsigned(den));
        IF vwtemp(n + 1) = '1' THEN
          vw := vwtemp;
          vqm(i) := '1';
        END IF;

      END IF;
    END LOOP;

    IF vw(n + 1) = '0' THEN
      q <= STD_LOGIC_VECTOR(unsigned(vqp) - unsigned(vqm));
    ELSE
      q <= STD_LOGIC_VECTOR(unsigned(vqp) - unsigned(vqm) - 1);
    END IF;
  END PROCESS;

END srt2;