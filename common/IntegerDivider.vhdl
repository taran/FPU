-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    IntegerDivider                                             --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Integer Divider  (simple)                                  --
--                                                                            --
--------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;

ENTITY IntegerDivider IS
  GENERIC (n : POSITIVE := 1);
  PORT (
    num, den : IN STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
    q : OUT STD_LOGIC_VECTOR(n - 1 DOWNTO 0));
END ENTITY IntegerDivider;

ARCHITECTURE naive OF IntegerDivider IS

BEGIN
  div : PROCESS (num, den) IS
    VARIABLE vaq : STD_LOGIC_VECTOR(2 * n - 1 DOWNTO 0);
    VARIABLE vm : STD_LOGIC_VECTOR(n - 1 DOWNTO 0);

  BEGIN
    vm(n - 1 DOWNTO 0) := den;
    vaq(2 * n - 1 DOWNTO n) := (OTHERS => '0');
    vaq(n - 1 DOWNTO 0) := num;

    FOR i IN n - 1 DOWNTO 0 LOOP

      IF vaq(2 * n - 1) = '0' THEN
        vaq := STD_LOGIC_VECTOR(shift_left(unsigned(vaq), 1));
        vaq(2 * n - 1 DOWNTO n) := STD_LOGIC_VECTOR(unsigned(vaq(2 * n - 1 DOWNTO n)) - unsigned(vm));
      ELSE
        vaq := STD_LOGIC_VECTOR(shift_left(unsigned(vaq), 1));
        vaq(2 * n - 1 DOWNTO n) := STD_LOGIC_VECTOR(unsigned(vaq(2 * n - 1 DOWNTO n)) + unsigned(vm));
      END IF;

      IF vaq(2 * n - 1) = '0' THEN
        vaq(0) := '1';
      ELSE
        vaq(0) := '0';
      END IF;
    END LOOP;

    q <= vaq(n - 1 DOWNTO 0);
  END PROCESS;

END naive;