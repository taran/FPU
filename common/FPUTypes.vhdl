-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    fputypes                                                   --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    fputypes package                                           --
--                                                                            --
--------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

PACKAGE fputypes IS

  TYPE roundingMode IS (toZero, toNearest);
  TYPE divRadix IS (radix2, radix4);
  TYPE multiplier IS (naive, dadda, mul);
  TYPE subnormalHandling IS (subOn, subOff);

  --10 bits float for testing
  -- CONSTANT e : INTEGER := 4;
  -- CONSTANT p : INTEGER := 6;

  --binary32 ieee-754
  CONSTANT e : INTEGER := 8;
  CONSTANT p : INTEGER := 24;

  -- constant rMode : roundingMode := toZero;
  CONSTANT rMode : roundingMode := toNearest;

  CONSTANT divArch : divRadix := radix4;
  CONSTANT mulArch : multiplier := mul; -- dadda, naive, mul
  CONSTANT subHandling : subnormalHandling := subOn;


  CONSTANT infExp : STD_LOGIC_VECTOR(e + 1 DOWNTO 0) := (e + 1 => '0', e => '0', OTHERS => '1');
  CONSTANT NbitMul : INTEGER := p;
  CONSTANT minusBias : STD_LOGIC_VECTOR(e + 1 DOWNTO 0) := (0 => '1', e - 1 => '1', e => '1', e + 1 => '1', OTHERS => '0');
  CONSTANT minusBiasP1 : STD_LOGIC_VECTOR(e + 1 DOWNTO 0) := (1 => '1', e - 1 => '1', e => '1', e + 1 => '1', OTHERS => '0');
  CONSTANT bep2Minus2Be : STD_LOGIC_VECTOR(e + 1 DOWNTO 0) := (0 => '1', e => '1', OTHERS => '0');
  CONSTANT beMinusBep2 : STD_LOGIC_VECTOR(e + 1 DOWNTO 0) := (0 => '1', e - 1 => '1', e + 1 => '1', OTHERS => '0');
  TYPE productArray IS ARRAY (NbitMul - 1 DOWNTO 0) OF STD_LOGIC_VECTOR(NbitMul * 2 - 1 DOWNTO 0);
END fputypes;