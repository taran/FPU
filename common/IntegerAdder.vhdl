-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    IntegerAdder                                               --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Integer Adder  (Ripple Cary)                               --
--                                                                            --
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity IntegerAdder is
	generic(n: positive:=1);
	port (a, b: in std_logic_vector(n-1 downto 0);
      c_in: in std_logic;
      sum: out std_logic_vector(n-1 downto 0);
      carry: out std_logic);
end entity IntegerAdder;

architecture structural of IntegerAdder is

  -- component FullAdder is
  --   port (a, c_in, b: in std_logic; 
  --         sum, carry: out std_logic);
  -- end component FullAdder;

  -- signal sc_in : std_logic_vector(n downto 0);
  -- signal ssum : std_logic_vector(n-1 downto 0);
  signal ssum : UNSIGNED(n downto 0);
  signal uc_in : UNSIGNED(n downto 0);

begin
  -- RCA Adder
  -- NFA: for i in 0 to n-1 generate
  --   FA: FullAdder port map (a=>a(i),b=>b(i),c_in=>sc_in(i),carry=>sc_in(i+1),sum=>ssum(i));
  -- end generate;

  -- carry<=sc_in(n); --unsigned
  -- sc_in(0)<=c_in;
  -- sum<=ssum;

  -- Normal adder
  uc_in(n downto 1) <= (others => '0');
  uc_in(0) <= c_in;
  ssum <= UNSIGNED('0' & a) + UNSIGNED('0' & b) + uc_in;
  sum <= STD_LOGIC_VECTOR(ssum(n-1 downto 0));
  carry <= ssum(n);

end structural;

