-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    PartialProducts                                            --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Partial Products for Integer Mutliplier                    --
--                                                                            --
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

library work;
use work.fputypes.all;

entity PartialProducts is
	port (a, b: in std_logic_vector(NbitMul-1 downto 0);
      products: out productArray);
end entity PartialProducts;

architecture structural of PartialProducts is

begin
  process(a,b)
  variable vLine : std_logic_vector(NbitMul*2-1 downto 0);
  begin
    for i in 0 to NbitMul-1 loop
      vLine := (others=>'0');
      if b(i)='1' then
        vLine(i+NbitMul-1 downto i) := a;
      end if;
      products(i) <= vLine;
    end loop;
    
  end process;

end structural;

