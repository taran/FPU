-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    Denormalizer                                               --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Floating-Point Denormalizer                                --
--                                                                            --
--------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.std_logic_misc.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.fputypes.ALL;

ENTITY Denormalizer IS
  PORT (
    significandNorm : IN STD_LOGIC_VECTOR(2 * p - 2 DOWNTO 0);
    expNorm : IN STD_LOGIC_VECTOR(e + 1 DOWNTO 0);
    significand : OUT STD_LOGIC_VECTOR(p - 2 DOWNTO 0);
    exp : OUT STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
    overflow, subnormal, inexact : OUT STD_LOGIC);
END ENTITY Denormalizer;

ARCHITECTURE structural OF Denormalizer IS

  COMPONENT SignificandRounding IS
    PORT (
      sticky, rbit, gbit : IN STD_LOGIC;
      increment : OUT STD_LOGIC);
  END COMPONENT SignificandRounding;

  COMPONENT IntegerAdder IS
    GENERIC (n : POSITIVE);
    PORT (
      a, b : IN STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      c_in : IN STD_LOGIC;
      sum : OUT STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      carry : OUT STD_LOGIC);
  END COMPONENT IntegerAdder;

  SIGNAL sExpDenorm, sExpAfterRound : STD_LOGIC_VECTOR(e + 1 DOWNTO 0);
  SIGNAL sSignificandDenorm : STD_LOGIC_VECTOR(3 * p - 3 DOWNTO 0);

  SIGNAL sRoundInc, sSticky, sR, sG, sCout : STD_LOGIC;

BEGIN
  ADDINCSIG : IntegerAdder GENERIC MAP(n => p - 1)
  PORT MAP(a => sSignificandDenorm(3 * p - 3 DOWNTO 2 * p - 1), b => (OTHERS => '0'), c_in => sRoundInc, sum => significand, carry => sCout);

  ROUND : SignificandRounding PORT MAP(sticky => sSticky, rbit => sR, gbit => sG, increment => sRoundInc);

  ADDINCEXP : IntegerAdder GENERIC MAP(n => e + 2)
  PORT MAP(a => sExpDenorm, b => (OTHERS => '0'), c_in => sCout, sum => sExpAfterRound);
  sExpDenorm <= STD_LOGIC_VECTOR(unsigned(expNorm));

  exp <= sExpAfterRound(e - 1 DOWNTO 0);

  sG <= sSignificandDenorm(2 * p - 1);
  sR <= sSignificandDenorm(2 * p - 2);
  sSticky <= or_reduce(sSignificandDenorm(2 * p - 3 DOWNTO 0));

  inexact <= or_reduce(sSignificandDenorm(2 * p - 2 DOWNTO 0));
  subnormal <= '1' WHEN signed(sExpAfterRound) <= 0 ELSE
    '0';
  overflow <= '1' WHEN signed(sExpAfterRound) >= signed(infExp) ELSE
    '0';

  denorm : PROCESS (sExpDenorm, significandNorm) IS
    VARIABLE significandDenorm : STD_LOGIC_VECTOR(3 * p - 3 DOWNTO 0);
    VARIABLE significandDenormBS : STD_LOGIC_VECTOR(3 * p - 3 DOWNTO 0);
    VARIABLE shift : INTEGER RANGE p - 1 DOWNTO 0;
    VARIABLE sub : STD_LOGIC;

  BEGIN
    IF to_integer(signed(sExpDenorm)) <= 0 THEN
      sub := '1';
    ELSE
      sub := '0';
    END IF;
    IF sub = '1' AND -to_integer(signed(sExpDenorm)) <= p - 1 THEN
      shift := - to_integer(signed(sExpDenorm));
      significandDenormBS(3 * p - 3) := '1';
      significandDenormBS(3 * p - 4 DOWNTO p - 2) := significandNorm;
      significandDenormBS(p - 1 DOWNTO 0) := (OTHERS => '0');

      significandDenorm := STD_LOGIC_VECTOR(shift_right(unsigned(significandDenormBS), shift));
    ELSIF sub = '0' THEN
      significandDenorm(3 * p - 3 DOWNTO p - 1) := significandNorm(2 * p - 2 DOWNTO 0);
      significandDenorm(p - 2 DOWNTO 0) := (OTHERS => '0');
    ELSE
      significandDenorm := (OTHERS => '0');
    END IF;
    sSignificandDenorm <= significandDenorm;
  END PROCESS;

END structural;