-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    SignificandRounding                                        --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Significand Rounding                                       --
--                                                                            --
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

library work;
use work.fputypes.all;

entity SignificandRounding is
	port (sticky,rbit,gbit: in std_logic;
      increment: out std_logic);
end entity SignificandRounding;

architecture behavioral of SignificandRounding is

begin
  ROUNDZ: if (rMode = toZero) generate
    increment<= '0';
  end generate;

  ROUNDN: if (rMode = toNearest) generate
    increment<= '1' when ((rbit and sticky) or (rbit and not sticky and gbit))='1' else '0';
  end generate;
  
end behavioral;