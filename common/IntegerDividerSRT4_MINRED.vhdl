-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    IntegerDividerSRT4_MINRED                                  --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Integer Divider  (srt division radix 4 MINRED)             --
--                                                                            --
--------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;


LIBRARY work;

ENTITY IntegerDividerSRT4_MINRED IS
  GENERIC (n : POSITIVE := 1);
  PORT (
    num, den : IN STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
    q : OUT STD_LOGIC_VECTOR(n - 1 DOWNTO 0));
END ENTITY IntegerDividerSRT4_MINRED;


ARCHITECTURE srt4_minred OF IntegerDividerSRT4_MINRED IS --srt division radix 4 using quotient digit set {-2,-1,0,1,2}, den*2/3 > num

BEGIN
  divsrt4 : PROCESS (num, den) IS
    VARIABLE vqm : STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
    VARIABLE vqp : STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
    VARIABLE vq : STD_LOGIC_VECTOR(n - 1 DOWNTO 0);

    VARIABLE vw : STD_LOGIC_VECTOR(n + 2 DOWNTO 0);
    VARIABLE vwtemp1 : STD_LOGIC_VECTOR(n + 2 DOWNTO 0);
    VARIABLE vwtemp2 : STD_LOGIC_VECTOR(n + 2 DOWNTO 0);
    VARIABLE vd15 : STD_LOGIC_VECTOR(n DOWNTO 0); --den * 1.5
    VARIABLE vd05 : STD_LOGIC_VECTOR(n - 1 DOWNTO 0); --den * 0.5

  BEGIN
    vw(n + 2 DOWNTO n) := (OTHERS => '0');
    vw(n - 1 DOWNTO 0) := num;
    vqm := (OTHERS => '0');
    vqp := (OTHERS => '0');

    vd05 := STD_LOGIC_VECTOR(shift_right(unsigned(den), 1));
    vd15 := STD_LOGIC_VECTOR(unsigned(vd05) + unsigned('0' & den));

    FOR i IN (n/2) - 1 DOWNTO 0 LOOP
      vw := STD_LOGIC_VECTOR(shift_left(unsigned(vw), 2));

      IF vw(n + 2) = '0' THEN
        vwtemp1 := STD_LOGIC_VECTOR(unsigned(vw) - unsigned(vd15));
        vwtemp2 := STD_LOGIC_VECTOR(unsigned(vw) - unsigned(vd05));
        IF vwtemp1(n + 2) = '0' THEN
          vw := STD_LOGIC_VECTOR(unsigned(vw) - unsigned(den & '0'));
          vqp(2 * i + 1 DOWNTO 2 * i) := "10";
        ELSIF vwtemp2(n + 2) = '0' THEN
          vw := STD_LOGIC_VECTOR(unsigned(vw) - unsigned(den));
          vqp(2 * i + 1 DOWNTO 2 * i) := "01";
        END IF;

      ELSE

        vwtemp1 := STD_LOGIC_VECTOR(unsigned(vw) + unsigned(vd15));
        vwtemp2 := STD_LOGIC_VECTOR(unsigned(vw) + unsigned(vd05));
        IF vwtemp1(n + 2) = '1' THEN
          vw := STD_LOGIC_VECTOR(unsigned(vw) + unsigned(den & '0'));
          vqm(2 * i + 1 DOWNTO 2 * i) := "10";
        ELSIF vwtemp2(n + 2) = '1' THEN
          vw := STD_LOGIC_VECTOR(unsigned(vw) + unsigned(den));
          vqm(2 * i + 1 DOWNTO 2 * i) := "01";
        END IF;

      END IF;
    END LOOP;

    IF vw(n + 1) = '0' THEN
      q <= STD_LOGIC_VECTOR(unsigned(vqp) - unsigned(vqm));
    ELSE
      q <= STD_LOGIC_VECTOR(unsigned(vqp) - unsigned(vqm) - 1);
    END IF;
  END PROCESS;

END srt4_minred;