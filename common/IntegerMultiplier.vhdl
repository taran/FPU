-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    IntegerMultiplier                                          --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Integer Multiplier  (classical)                            --
--                                                                            --
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

library work;
use work.fputypes.all;

entity IntegerMultiplier is
	port (a, b: in std_logic_vector(NbitMul-1 downto 0);
      result: out std_logic_vector(2*NbitMul-1 downto 0));
end entity IntegerMultiplier;

architecture naive of IntegerMultiplier is

  component PartialProducts is
    port (a, b: in std_logic_vector(NbitMul-1 downto 0);
        products: out productArray);
  end component PartialProducts;

  component IntegerAdder is
    generic(n: positive);
    port (a, b: in std_logic_vector(n-1 downto 0);
        c_in: in std_logic;
        sum: out std_logic_vector(n-1 downto 0);
        carry: out std_logic);
  end component IntegerAdder;

  signal sproducts: productArray;
  signal ssubsum: productArray;
begin

  PP: PartialProducts port map(a=>a,b=>b,products=>sproducts);

  NINTA: for i in 0 to NbitMul-2 generate
    INTA: IntegerAdder generic map(n=>NbitMul*2)
      port map (a=>ssubsum(i),b=>sproducts(i+1),c_in=>'0',sum=>ssubsum(i+1));
  end generate;

  ssubsum(0)<=sproducts(0);
  result<=ssubsum(NbitMul-1);


end naive;
