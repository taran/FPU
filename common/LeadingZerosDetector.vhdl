-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    LeadingZerosDetector                                       --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Leading Zeros Detector                                     --
--                                                                            --
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

library work;
use work.fputypes.all;

entity LeadingZerosDetector is
  generic(n: positive:=1);
	port (x: in std_logic_vector(n-1 downto 0);
      shift: out integer range n-1 downto 0;
      zero: out std_logic);
end entity LeadingZerosDetector;

architecture structural of LeadingZerosDetector is

begin
  shifter: process(x) is
    variable firstOnePos: integer range n-1 downto 0;

    begin
      firstOnePos := 0;
      if or_reduce(x)='0' then
        shift <= 0;
        zero <= '1';
      else
        for i in 0 to n-1 loop
          if x(i)='1' then
            firstOnePos := i;
          end if;
        end loop;
        shift <= n-1-firstOnePos;
        zero <= '0';
      end if;
      
    end process;

end structural;