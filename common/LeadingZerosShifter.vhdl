-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    LeadingZerosShifter                                        --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Leading Zeros Shifter                                      --
--                                                                            --
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.fputypes.all;

entity LeadingZerosShifter is
  generic(n: positive:=1);
	port (x: in std_logic_vector(n-1 downto 0);
      y: out std_logic_vector(n-1 downto 0);
      shift: out integer range n-1 downto 0;
      zero: out std_logic);
end entity LeadingZerosShifter;

architecture structural of LeadingZerosShifter is
  component LeadingZerosDetector is
    generic(n: positive);
    port (x: in std_logic_vector(n-1 downto 0);
        shift: out integer range n-1 downto 0;
        zero: out std_logic);
  end component LeadingZerosDetector;

  signal sShift: integer range n-1 downto 0;

begin

  LZD: LeadingZerosDetector generic map(n=>n)
    port map(x=>x, shift=>sShift, zero=>zero);

  y <= std_logic_vector(shift_left(unsigned(x), sShift));
  shift <= sShift;

end structural;