-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    SignificandSubstractor                                     --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Floating-Point Adder Significand Substractor Unit          --
--                                                                            --
--------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.std_logic_misc.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.fputypes.ALL;

ENTITY SignificandSubstractor IS
  PORT (
    ea : IN STD_LOGIC_VECTOR(e-1 DOWNTO 0);
    ma, mb : IN STD_LOGIC_VECTOR(2*p DOWNTO 0);
    ez : OUT STD_LOGIC_VECTOR(e-1 DOWNTO 0);
    mz : OUT STD_LOGIC_VECTOR(2*p DOWNTO 0);
    zero : OUT STD_LOGIC);
END ENTITY SignificandSubstractor;

ARCHITECTURE structural OF SignificandSubstractor IS

  COMPONENT LeadingZerosDetector IS
    GENERIC (n : POSITIVE);
    PORT (
      x : IN STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      shift : OUT INTEGER RANGE n - 1 DOWNTO 0;
      zero : OUT STD_LOGIC);
  END COMPONENT LeadingZerosDetector;

  SIGNAL sShiftVal, sFinalShift : INTEGER RANGE 2*p DOWNTO 0;
  SIGNAL sEz : STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
  SIGNAL sSubResultBeforeShift, sSubResult : STD_LOGIC_VECTOR(2*p DOWNTO 0);

BEGIN

  LZD : LeadingZerosDetector GENERIC MAP(n => 2*p+1)
  PORT MAP(x => sSubResultBeforeShift, shift => sShiftVal, zero => zero);

  sSubResultBeforeShift <= STD_LOGIC_VECTOR(unsigned(ma) - unsigned(mb));
  sFinalShift <= sShiftVal WHEN sShiftVal <= unsigned(ea) ELSE
    to_integer(unsigned(ea));
  sSubResult <= STD_LOGIC_VECTOR(shift_left(unsigned(sSubResultBeforeShift), sFinalShift));

  mz <= sSubResult(2 * p - 1 DOWNTO 0) & '0' WHEN unsigned(sEz) > 0 ELSE
    sSubResult(2 * p DOWNTO 0);
  sEz <= STD_LOGIC_VECTOR(unsigned(ea) - sFinalShift);
  ez <= sEz;

END structural;