-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    SignificandAdder                                           --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Floating-Point Adder Significand Adder Unit                --
--                                                                            --
--------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.std_logic_misc.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.fputypes.ALL;

ENTITY SignificandAdder IS
  PORT (
    ea : IN STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
    ma, mb : IN STD_LOGIC_VECTOR(2 * p DOWNTO 0);
    ez : OUT STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
    mz : OUT STD_LOGIC_VECTOR(2 * p DOWNTO 0);
    overflow : OUT STD_LOGIC);
END ENTITY SignificandAdder;

ARCHITECTURE structural OF SignificandAdder IS

  COMPONENT IntegerAdder IS
    GENERIC (n : POSITIVE);
    PORT (
      a, b : IN STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      c_in : IN STD_LOGIC;
      sum : OUT STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      carry : OUT STD_LOGIC);
  END COMPONENT IntegerAdder;

  SIGNAL sEz : STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
  SIGNAL sAddResult : STD_LOGIC_VECTOR(2 * p DOWNTO 0);
  SIGNAL sCoutAddSig, sCoutAddCout : STD_LOGIC;

BEGIN

  ADDSIG : IntegerAdder GENERIC MAP(n => 2 * p + 1)
  PORT MAP(a => ma, b => mb, c_in => '0', sum => sAddResult, carry => sCoutAddSig);

  ADDCOUT : IntegerAdder GENERIC MAP(n => e)
  PORT MAP(a => ea, b => (OTHERS => '0'), c_in => sCoutAddSig, sum => sEz, carry => sCoutAddCout);

  overflow <= sCoutAddCout OR and_reduce(sEz);
  mz <= sAddResult(2 * p - 1 DOWNTO 0) & '0' WHEN sCoutAddSig = '0' AND unsigned(ea) > 0 ELSE
    sAddResult(2 * p DOWNTO 0);
  ez <= sEz;

END structural;