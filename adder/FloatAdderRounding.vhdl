-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    FloatAdderRounding                                         --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Floating-Point Adder Rounding Unit                         --
--                                                                            --
--------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.std_logic_misc.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.fputypes.ALL;

ENTITY FloatAdderRounding IS
  PORT (
    expBeforeRound : IN STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
    sigBeforeRound : IN STD_LOGIC_VECTOR(2 * p DOWNTO 0);
    exp : OUT STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
    sig : OUT STD_LOGIC_VECTOR(p - 2 DOWNTO 0);
    overflow, inexact, underflow : OUT STD_LOGIC);
END ENTITY FloatAdderRounding;

ARCHITECTURE structural OF FloatAdderRounding IS

  COMPONENT SignificandRounding IS
    PORT (
      sticky, rbit, gbit : IN STD_LOGIC;
      increment : OUT STD_LOGIC);
  END COMPONENT SignificandRounding;

  COMPONENT IntegerAdder IS
    GENERIC (n : POSITIVE);
    PORT (
      a, b : IN STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      c_in : IN STD_LOGIC;
      sum : OUT STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      carry : OUT STD_LOGIC);
  END COMPONENT IntegerAdder;

  SIGNAL sSigTrunc, sSigTruncP1 : STD_LOGIC_VECTOR(p - 2 DOWNTO 0);
  SIGNAL sExpBeforeRoundP1, sExp : STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
  SIGNAL sSubnormal, sOverflow, sInexact, sRoundInc, sExpInc, sCoutSig, sSticky, sR, sG : STD_LOGIC;

BEGIN

  ADDINCSIG : IntegerAdder GENERIC MAP(n => p - 1)
  PORT MAP(a => sSigTrunc, b => (OTHERS => '0'), c_in => '1', sum => sSigTruncP1, carry => sCoutSig);

  ADDINCEXP : IntegerAdder GENERIC MAP(n => e)
  PORT MAP(a => expBeforeRound, b => (OTHERS => '0'), c_in => '1', sum => sExpBeforeRoundP1, carry => sOverflow);

  ROUND : SignificandRounding PORT MAP(sticky => sSticky, rbit => sR, gbit => sG, increment => sRoundInc);

  sSigTrunc <= sigBeforeRound(2 * p DOWNTO p + 2);

  sG <= sigBeforeRound(p + 2);
  sR <= sigBeforeRound(p + 1);
  sSticky <= or_reduce(sigBeforeRound(p DOWNTO 0));
  sExp <= sExpBeforeRoundP1 WHEN sCoutSig = '1' ELSE
    expBeforeRound;
  exp <= sExp;
  sig <= sSigTruncP1 WHEN sRoundInc = '1' ELSE
    sSigTrunc;

  sSubnormal <= '1' when (or_reduce(sExp) = '0') AND (sOverflow='0') else '0';
  sInexact <= sSticky OR sR;
  overflow <= sOverflow;
  inexact <= sInexact;
  underflow <= sSubnormal AND sInexact;
END structural;