-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    FloatAdder                                                 --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Floating-Point Adder Top Level                             --
--                                                                            --
--------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.std_logic_misc.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.fputypes.ALL;

ENTITY FloatAdder IS
  PORT (
    x, y : IN STD_LOGIC_VECTOR(e + p - 1 DOWNTO 0);
    z : OUT STD_LOGIC_VECTOR(e + p - 1 DOWNTO 0);
    overflow, underflow, inexact, zero : OUT STD_LOGIC);
END ENTITY FloatAdder;

ARCHITECTURE structural OF FloatAdder IS

  COMPONENT OperandSwitch IS
    PORT (
      x, y : IN STD_LOGIC_VECTOR(e + p - 1 DOWNTO 0);
      ea, eb : OUT STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
      ma, mb : OUT STD_LOGIC_VECTOR(p - 2 DOWNTO 0);
      sa, sb : OUT STD_LOGIC);
  END COMPONENT OperandSwitch;

  COMPONENT SignificandAdder IS
    PORT (
      ea : IN STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
      ma, mb : IN STD_LOGIC_VECTOR(2 * p DOWNTO 0);
      ez : OUT STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
      mz : OUT STD_LOGIC_VECTOR(2 * p DOWNTO 0);
      overflow : OUT STD_LOGIC);
  END COMPONENT SignificandAdder;

  COMPONENT SignificandSubstractor IS
    PORT (
      ea : IN STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
      ma, mb : IN STD_LOGIC_VECTOR(2 * p DOWNTO 0);
      ez : OUT STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
      mz : OUT STD_LOGIC_VECTOR(2 * p DOWNTO 0);
      zero : OUT STD_LOGIC);
  END COMPONENT SignificandSubstractor;

  COMPONENT FloatAdderRounding IS
    PORT (
      expBeforeRound : IN STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
      sigBeforeRound : IN STD_LOGIC_VECTOR(2 * p DOWNTO 0);
      exp : OUT STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
      sig : OUT STD_LOGIC_VECTOR(p - 2 DOWNTO 0);
      overflow, inexact, underflow : OUT STD_LOGIC);
  END COMPONENT FloatAdderRounding;

  SIGNAL sNaN, sInfP, sInfN, sInf, sZero, sOverflow : STD_LOGIC;
  SIGNAL sXNaN, sYNaN, sXInfP, sYInfP, sXInfN, sYInfN, sXZero, sYZero, sZeroSubRes, sOverflowAddRes, sOverflowRound : STD_LOGIC;
  SIGNAL sEz, sEzBeforeRound, sEzAdd, sEzSub, sEa, sEb, sEdiff : STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
  SIGNAL sOpA, sOpAN, sOpAD, sOpB, sOpBTrunc, sOpBbeforeShiftN, sOpBbeforeShiftD, sOpBbeforeShift : STD_LOGIC_VECTOR(2 * p DOWNTO 0);
  SIGNAL sMz, sMa, sMb : STD_LOGIC_VECTOR(p - 2 DOWNTO 0);
  SIGNAL sMzBeforeRound, sMzAdd, sMzSub : STD_LOGIC_VECTOR(2 * p DOWNTO 0);
  SIGNAL sSa, sSb : STD_LOGIC;
  SIGNAL sSubstract : STD_LOGIC;

BEGIN

  SWITCH : OperandSwitch PORT MAP(x => x, y => y, ea => sEa, eb => sEb, ma => sMa, mb => sMb, sa => sSa, sb => sSb);

  SIGADD : SignificandAdder PORT MAP(ea => sEa, ma => sOpA, mb => sOpB, ez => sEzAdd, mz => sMzAdd, overflow => sOverflowAddRes);

  SIGSUB : SignificandSubstractor PORT MAP(ea => sEa, ma => sOpA, mb => sOpB, ez => sEzSub, mz => sMzSub, zero => sZeroSubRes);
  sSubstract <= sSa XOR sSb;

  ROUND : FloatAdderRounding PORT MAP(
    expBeforeRound => sEzBeforeRound, sigBeforeRound => sMzBeforeRound, exp => sEz, sig => sMz, overflow => sOverflowRound, inexact => inexact, underflow => underflow);

  sOverflow <= sOverflowAddRes AND NOT sSubstract;
  sEdiff <= STD_LOGIC_VECTOR(unsigned(sEa) - unsigned(sEb));
  sOpAN(2 * p) <= '1' WHEN unsigned(sEa) > 0 ELSE
  '0';
  sOpAN(2 * p - 1 DOWNTO p + 1) <= sMa;
  sOpAN(p DOWNTO 0) <= (OTHERS => '0');

  sOpAD(2 * p DOWNTO p + 2) <= sMa;
  sOpAD(p + 1 DOWNTO 0) <= (OTHERS => '0');

  sOpA <= sOpAN WHEN unsigned(sEa) > 0 ELSE
    sOpAD;

  sOpBbeforeShiftN(2 * p) <= '1' WHEN unsigned(sEb) > 0 ELSE
  '0';
  sOpBbeforeShiftN(2 * p - 1 DOWNTO p + 1) <= sMb;
  sOpBbeforeShiftN(p DOWNTO 0) <= (OTHERS => '0');

  sOpBbeforeShiftD(2 * p DOWNTO p + 2) <= sMb;
  sOpBbeforeShiftD(p + 1 DOWNTO 0) <= (OTHERS => '0');

  sOpBbeforeShift <= sOpBbeforeShiftN WHEN unsigned(sEb) > 0 ELSE
    sOpBbeforeShiftD;

  sOpBTrunc <= STD_LOGIC_VECTOR(shift_right(unsigned(sOpBbeforeShift), to_integer(unsigned(sEdiff))));
  sOpB <= sOpBTrunc WHEN sSubstract = '0' OR or_reduce(sOpBTrunc) = '1' OR or_reduce(sOpBbeforeShift) = '0' ELSE
    (0 => '1', OTHERS => '0');

  sMzBeforeRound <= sMzAdd WHEN sSubstract = '0' ELSE
    sMzSub;
  sEzBeforeRound <= sEzAdd WHEN sSubstract = '0' ELSE
    sEzSub;

  z(p + e - 1) <= '0' WHEN sNaN = '1' OR sInfP = '1' OR (sZeroSubRes = '1' AND sSubstract = '1') ELSE
  '1' WHEN sInfN = '1' OR sSa = '1' ELSE
  '0';

  ROUNDN : IF (rMode = toNearest) GENERATE
    --SIGNIFICAND
    z(p - 2 DOWNTO 0) <= (0 => '1', OTHERS => '0') WHEN sNaN = '1' ELSE
    (OTHERS => '0') WHEN sInf = '1' ELSE
    (OTHERS => '0') WHEN sOverflow = '1' ELSE
    (OTHERS => '0') WHEN sZero = '1' ELSE
    sMz;

    --EXPONENT
    z(p + e - 2 DOWNTO p - 1) <= (OTHERS => '1') WHEN sNaN = '1' OR sInf = '1' ELSE
    (OTHERS => '1') WHEN sOverflow = '1' ELSE
    (OTHERS => '0') WHEN sZero = '1' ELSE
    sEz;

  END GENERATE;

  ROUNDZ : IF (rMode = toZero) GENERATE
    --SIGNIFICAND
    z(p - 2 DOWNTO 0) <= (0 => '1', OTHERS => '0') WHEN sNaN = '1' ELSE
    (OTHERS => '0') WHEN sInf = '1' ELSE
    (OTHERS => '1') WHEN sOverflow = '1' ELSE
    (OTHERS => '0') WHEN sZero = '1' ELSE
    sMz;

    --EXPONENT
    z(p + e - 2 DOWNTO p - 1) <= (OTHERS => '1') WHEN sNaN = '1' OR sInf = '1' ELSE
    (p - 1 => '0', OTHERS => '1') WHEN sOverflow = '1' ELSE
    (OTHERS => '0') WHEN sZero = '1' ELSE
    sEz;
  END GENERATE;

  --NAN/INF/ZERO CASES
  sXNaN <= '1' WHEN and_reduce(x(e + p - 2 DOWNTO p - 1)) = '1' AND or_reduce(x(p - 2 DOWNTO 0)) = '1' ELSE
    '0';
  sYNaN <= '1' WHEN and_reduce(y(e + p - 2 DOWNTO p - 1)) = '1' AND or_reduce(y(p - 2 DOWNTO 0)) = '1' ELSE
    '0';
  sXInfP <= '1' WHEN and_reduce(x(e + p - 2 DOWNTO p - 1)) = '1' AND or_reduce(x(p - 2 DOWNTO 0)) = '0' AND x(e + p - 1) = '0' ELSE
    '0';
  sXInfN <= '1' WHEN and_reduce(x(e + p - 2 DOWNTO p - 1)) = '1' AND or_reduce(x(p - 2 DOWNTO 0)) = '0' AND x(e + p - 1) = '1' ELSE
    '0';
  sYInfP <= '1' WHEN and_reduce(y(e + p - 2 DOWNTO p - 1)) = '1' AND or_reduce(y(p - 2 DOWNTO 0)) = '0' AND y(e + p - 1) = '0' ELSE
    '0';
  sYInfN <= '1' WHEN and_reduce(y(e + p - 2 DOWNTO p - 1)) = '1' AND or_reduce(y(p - 2 DOWNTO 0)) = '0' AND y(e + p - 1) = '1' ELSE
    '0';
  sXZero <= '1' WHEN or_reduce(x(e + p - 2 DOWNTO 0)) = '0' ELSE
    '0';
  sYZero <= '1' WHEN or_reduce(y(e + p - 2 DOWNTO 0)) = '0' ELSE
    '0';

  sNaN <= sXNaN OR sYNaN OR (sXInfP AND sYInfN) OR (sXInfN AND sYInfP);
  sInfP <= (sXInfP AND NOT sYInfN) OR (sYInfP AND NOT sXInfN);
  sInfN <= (sXInfN AND NOT sYInfP) OR (sYInfN AND NOT sXInfP);
  sInf <= sInfP OR sInfN;
  sZero <= ((sXZero AND sYZero) OR (sZeroSubRes AND sSubstract)) AND NOT sNaN;

  zero <= sZero;
  overflow <= sOverflow;
END structural;