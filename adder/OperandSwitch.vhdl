-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    OperandSwitch                                              --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Floating-Point Adder Operand Switch Unit                   --
--                                                                            --
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

library work;
use work.fputypes.all;

entity OperandSwitch is
	port (x, y: in std_logic_vector(e+p-1 downto 0);
      ea, eb: out std_logic_vector(e-1 downto 0);
      ma,mb: out std_logic_vector(p-2 downto 0);
      sa, sb: out std_logic);
end entity OperandSwitch;

architecture structural of OperandSwitch is

  signal sEx, sEy: std_logic_vector(e-1 downto 0);
  signal sMx,sMy: std_logic_vector(p-2 downto 0);
  signal sSx, sSy: std_logic;
  signal sInvertOperand : std_logic;



begin

  sEx <= x(e+p-2 downto p-1);
  sEy <= y(e+p-2 downto p-1);
  sMx <= x(p-2 downto 0);
  sMy <= y(p-2 downto 0);
  sSx <= x(e+p-1);
  sSy <= y(e+p-1);

  sInvertOperand <= '1' when (unsigned(sEx)<unsigned(sEy) or (unsigned(sEx)=unsigned(sEy) and unsigned(sMx)<unsigned(sMy))) else '0';

  ea <= sEx when sInvertOperand='0' else sEy;
  eb <= sEy when sInvertOperand='0' else sEx;

  ma <= sMx when sInvertOperand='0' else sMy;
  mb <= sMy when sInvertOperand='0' else sMx;

  sa <= sSx when sInvertOperand='0' else sSy;
  sb <= sSy when sInvertOperand='0' else sSx;
end structural;