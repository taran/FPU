vlib work
vmap C28SOI_SC_12_CORE_LL /opt/DesignKit/cmos28fdsoi_10a/C28SOI_SC_12_CORE_LL/5.1-05/behaviour/C28SOI_SC_12_CORE_LL

vcom -explicit -work work ../synthesis/fmultiplier_gate.vhd
vcom -explicit -work work ../common/FPUTypes.vhdl
vcom -explicit -work work ../testbench/sim_FloatMultiplier.vhdl

vsim -t ps work.test_floatm

view structure
view signals
view wave

run 20000000 ns
