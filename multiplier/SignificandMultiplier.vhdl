-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    SignificandMultiplier                                      --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Significand Multiplier for Floating-Point Multiplier       --
--                                                                            --
--------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.std_logic_misc.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.fputypes.ALL;

ENTITY SignificandMultiplier IS
  PORT (
    x, y : IN STD_LOGIC_VECTOR(e + p - 1 DOWNTO 0);
    significand : OUT STD_LOGIC_VECTOR(2 * p - 2 DOWNTO 0);
    zero : OUT STD_LOGIC;
    shiftVal : OUT INTEGER RANGE 2 * p - 1 DOWNTO 0;
    expIncrement : OUT STD_LOGIC);
END ENTITY SignificandMultiplier;

ARCHITECTURE structural OF SignificandMultiplier IS

  COMPONENT IntegerAdder IS
    GENERIC (n : POSITIVE);
    PORT (
      a, b : IN STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      c_in : IN STD_LOGIC;
      sum : OUT STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      carry : OUT STD_LOGIC);
  END COMPONENT IntegerAdder;

  COMPONENT IntegerMultiplier IS
    PORT (
      a, b : IN STD_LOGIC_VECTOR(NbitMul - 1 DOWNTO 0);
      result : OUT STD_LOGIC_VECTOR(2 * NbitMul - 1 DOWNTO 0));
  END COMPONENT IntegerMultiplier;

  COMPONENT IntegerMultiplierMul IS
    PORT (
      a, b : IN STD_LOGIC_VECTOR(NbitMul - 1 DOWNTO 0);
      result : OUT STD_LOGIC_VECTOR(2 * NbitMul - 1 DOWNTO 0));
  END COMPONENT IntegerMultiplierMul;

  COMPONENT LeadingZerosShifter IS
    GENERIC (n : POSITIVE);
    PORT (
      x : IN STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      y : OUT STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      shift : OUT INTEGER RANGE n - 1 DOWNTO 0;
      zero : OUT STD_LOGIC);
  END COMPONENT LeadingZerosShifter;

  COMPONENT Mult_Dadda24 IS
    GENERIC (N : NATURAL := 24);
    PORT (
      x : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      y : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      z : OUT STD_LOGIC_VECTOR(2 * N - 1 DOWNTO 0));
  END COMPONENT Mult_Dadda24;

  SIGNAL sMulXBeforeShift, sMulYBeforeShift, sMulX, sMulY : STD_LOGIC_VECTOR(p - 1 DOWNTO 0);
  SIGNAL sMulResult : STD_LOGIC_VECTOR(2 * p - 1 DOWNTO 0);
  SIGNAL shiftValX, shiftValY : INTEGER RANGE p - 1 DOWNTO 0;
  SIGNAL sZeroX, sZeroY : STD_LOGIC;

  SIGNAL sXnorm, sYnorm : STD_LOGIC;

BEGIN
  DADDA_MULT : IF (mulArch = dadda) GENERATE

    MULT : Mult_Dadda24 PORT MAP(x => sMulX, y => sMulY, z => sMulResult);

  END GENERATE;

  NAIVE_MULT : IF (mulArch = naive) GENERATE

    MULT : IntegerMultiplier PORT MAP(a => sMulX, b => sMulY, result => sMulResult);

  END GENERATE;

  MUL_MULT : IF (mulArch = mul) GENERATE

    MULT : IntegerMultiplierMul PORT MAP(a => sMulX, b => sMulY, result => sMulResult);

  END GENERATE;

  SUB_HANDLED : IF (subHandling = subOn) GENERATE

    SHIFTERX : LeadingZerosShifter GENERIC MAP(n => p)
    PORT MAP(x => sMulXBeforeShift, y => sMulX, shift => shiftValX, zero => sZeroX);

    SHIFTERY : LeadingZerosShifter GENERIC MAP(n => p)
    PORT MAP(x => sMulYBeforeShift, y => sMulY, shift => shiftValY, zero => sZeroY);

    zero <= sZeroX OR sZeroY;

    shiftVal <= shiftValX + shiftValY;

  END GENERATE;

  SUB_NOT_HANDLED : IF (subHandling = subOff) GENERATE

    sMulX<=sMulXBeforeShift;
    sMulY<=sMulYBeforeShift;
    zero <= '0';

    shiftVal <= 0;

  END GENERATE;

  sXnorm <= or_reduce(x(p + e - 2 DOWNTO p - 1));
  sYnorm <= or_reduce(y(p + e - 2 DOWNTO p - 1));

  

  sMulXBeforeShift <= '1' & x(p - 2 DOWNTO 0) WHEN sXnorm = '1' ELSE
    '0' & x(p - 2 DOWNTO 0);
  sMulYBeforeShift <= '1' & y(p - 2 DOWNTO 0) WHEN sYnorm = '1' ELSE
    '0' & y(p - 2 DOWNTO 0);

  significand <= sMulResult(2 * p - 2 DOWNTO 0) WHEN sMulResult(2 * p - 1) = '1' ELSE
    sMulResult(2 * p - 3 DOWNTO 0) & '0';

  expIncrement <= '1' WHEN sMulResult(2 * p - 1) = '1' ELSE
    '0';

END structural;