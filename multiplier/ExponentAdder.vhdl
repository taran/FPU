-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    ExponentAdder                                              --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Exponent Adder for Floating-Point Multiplier               --
--                                                                            --
--------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.std_logic_misc.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.fputypes.ALL;

ENTITY ExponentAdder IS
  PORT (
    ex, ey : IN STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
    shiftVal : IN INTEGER RANGE 2 * p - 1 DOWNTO 0;
    expIncrement : IN STD_LOGIC;
    ez : OUT STD_LOGIC_VECTOR(e + 1 DOWNTO 0));
END ENTITY ExponentAdder;

ARCHITECTURE structural OF ExponentAdder IS

  COMPONENT IntegerAdder IS
    GENERIC (n : POSITIVE);
    PORT (
      a, b : IN STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      c_in : IN STD_LOGIC;
      sum : OUT STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      carry : OUT STD_LOGIC);
  END COMPONENT IntegerAdder;

  SIGNAL sEx, sEy : STD_LOGIC_VECTOR(e + 1 DOWNTO 0);
  SIGNAL sEzBeforeInc, sEzBeforeIncP1 : STD_LOGIC_VECTOR(e + 1 DOWNTO 0);

BEGIN

  sEx <= "00" & ex WHEN NOT or_reduce(ex) = '0' ELSE
    (0 => '1', OTHERS => '0');
  sEy <= "00" & ey WHEN NOT or_reduce(ey) = '0' ELSE
    (0 => '1', OTHERS => '0');
  sEzBeforeIncP1 <= STD_LOGIC_VECTOR(unsigned(sEx) + unsigned(sEy) + unsigned(minusBiasP1) - to_unsigned(shiftVal, e + 2));
  sEzBeforeInc <= STD_LOGIC_VECTOR(unsigned(sEzBeforeIncP1) - 1);

  ez <= sEzBeforeIncP1 WHEN expIncrement = '1' ELSE
    sEzBeforeInc;

END structural;