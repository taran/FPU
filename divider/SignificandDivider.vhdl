-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    SignificandDivider                                         --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Significand Divider for Floating-Point Divider             --
--                                                                            --
--------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.std_logic_misc.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.fputypes.ALL;

ENTITY SignificandDivider IS
  PORT (
    x, y : IN STD_LOGIC_VECTOR(e + p - 1 DOWNTO 0);
    significand : OUT STD_LOGIC_VECTOR(2 * p - 2 DOWNTO 0);
    zero : OUT STD_LOGIC;
    shiftVal : OUT INTEGER RANGE 2 * p - 1 DOWNTO 0;
    expIncrement : OUT STD_LOGIC);
END ENTITY SignificandDivider;

ARCHITECTURE structural OF SignificandDivider IS

  COMPONENT IntegerDividerSRT2 IS
    GENERIC (n : POSITIVE := 1);
    PORT (
      num, den : IN STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      q : OUT STD_LOGIC_VECTOR(n - 1 DOWNTO 0));
  END COMPONENT IntegerDividerSRT2;

  COMPONENT IntegerDividerSRT4_MAXRED IS
    GENERIC (n : POSITIVE := 1);
    PORT (
      num, den : IN STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      q : OUT STD_LOGIC_VECTOR(n - 1 DOWNTO 0));
  END COMPONENT IntegerDividerSRT4_MAXRED;

  COMPONENT IntegerDividerSRT4_MINRED IS
    GENERIC (n : POSITIVE := 1);
    PORT (
      num, den : IN STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      q : OUT STD_LOGIC_VECTOR(n - 1 DOWNTO 0));
  END COMPONENT IntegerDividerSRT4_MINRED;

  COMPONENT LeadingZerosShifter IS
    GENERIC (n : POSITIVE);
    PORT (
      x : IN STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      y : OUT STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      shift : OUT INTEGER RANGE n - 1 DOWNTO 0;
      zero : OUT STD_LOGIC);
  END COMPONENT LeadingZerosShifter;

  SIGNAL sDivXBeforeShift, sDivYBeforeShift, sDivX, sDivY : STD_LOGIC_VECTOR(p - 1 DOWNTO 0);
  SIGNAL sDivXl, sDivYl, sDivResult : STD_LOGIC_VECTOR(p * 2 + 1 DOWNTO 0);
  SIGNAL shiftValX, shiftValY : INTEGER RANGE p - 1 DOWNTO 0;

  SIGNAL sXnorm, sYnorm : STD_LOGIC;

BEGIN
  SRT4 : IF (divArch = radix4) GENERATE

    SRT4_Z : IF (rMode = toZero) GENERATE
      DIV : IntegerDividerSRT4_MAXRED GENERIC MAP(n => p + 2)
      PORT MAP(num => sDivXl(2 * p + 1 DOWNTO p), den => sDivYl(2 * p + 1 DOWNTO p), q => sDivResult(p * 2 + 1 DOWNTO p));
      sDivResult(p-1 DOWNTO 0) <= (OTHERS => '0');
    END GENERATE;

    SRT4_N : IF (rMode = toNearest) GENERATE
      DIV : IntegerDividerSRT4_MAXRED GENERIC MAP(n => p * 2 + 2)
      PORT MAP(num => sDivXl, den => sDivYl, q => sDivResult);
    END GENERATE;

  END GENERATE;

  SRT2 : IF (divArch = radix2) GENERATE

    SRT2_Z : IF (rMode = toZero) GENERATE
      DIV : IntegerDividerSRT2 GENERIC MAP(n => p + 1)
      PORT MAP(num => sDivXl(2 * p + 1 DOWNTO p + 1), den => sDivYl(2 * p + 1 DOWNTO p + 1), q => sDivResult(p * 2 + 1 DOWNTO p + 1));
      sDivResult(p DOWNTO 0) <= (OTHERS => '0');
    END GENERATE;

    SRT2_N : IF (rMode = toNearest) GENERATE
      DIV : IntegerDividerSRT2 GENERIC MAP(n => p * 2 + 2)
      PORT MAP(num => sDivXl, den => sDivYl, q => sDivResult);
    END GENERATE;

  END GENERATE;

  SUB_HANDLED : IF (subHandling = subOn) GENERATE

    SHIFTERX : LeadingZerosShifter GENERIC MAP(n => p)
    PORT MAP(x => sDivXBeforeShift, y => sDivX, shift => shiftValX, zero => zero);

    SHIFTERY : LeadingZerosShifter GENERIC MAP(n => p)
    PORT MAP(x => sDivYBeforeShift, y => sDivY, shift => shiftValY);

    shiftVal <= shiftValX - shiftValY + p;
  END GENERATE;

  SUB_NOT_HANDLED : IF (subHandling = subOff) GENERATE

    sDivX <= sDivXBeforeShift;
    sDivY <= sDivYBeforeShift;

    shiftVal <= p;

  END GENERATE;
  sDivXl(2 * p + 1) <= '0';
  sDivXl(2 * p DOWNTO p + 1) <= sDivX;
  sDivXl(p DOWNTO 0) <= (OTHERS => '0');

  sDivYl(2 * p + 1 DOWNTO p + 2) <= sDivY;
  sDivYl(p + 1 DOWNTO 0) <= (OTHERS => '0');

  sXnorm <= or_reduce(x(p + e - 2 DOWNTO p - 1));
  sYnorm <= or_reduce(y(p + e - 2 DOWNTO p - 1));

  sDivXBeforeShift <= '1' & x(p - 2 DOWNTO 0) WHEN sXnorm = '1' ELSE
    '0' & x(p - 2 DOWNTO 0);
  sDivYBeforeShift <= '1' & y(p - 2 DOWNTO 0) WHEN sYnorm = '1' ELSE
    '0' & y(p - 2 DOWNTO 0);

  significand <= sDivResult(2 * p DOWNTO 2) WHEN sDivResult(2 * p + 1) = '1' ELSE
    sDivResult(2 * p - 1 DOWNTO 1);

  expIncrement <= '1' WHEN sDivResult(2 * p + 1) = '1' ELSE
    '0';

END structural;