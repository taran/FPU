-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    FloatDivider                                               --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    Floating-Point Divider                                     --
--                                                                            --
--------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.std_logic_misc.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.fputypes.ALL;

ENTITY FloatDivider IS
  PORT (
    x, y : IN STD_LOGIC_VECTOR(e + p - 1 DOWNTO 0);
    z : OUT STD_LOGIC_VECTOR(e + p - 1 DOWNTO 0);
    overflow, underflow, inexact, zero : OUT STD_LOGIC);
END ENTITY FloatDivider;

ARCHITECTURE structural OF FloatDivider IS

  COMPONENT IntegerAdder IS
    GENERIC (n : POSITIVE);
    PORT (
      a, b : IN STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      c_in : IN STD_LOGIC;
      sum : OUT STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
      carry : OUT STD_LOGIC);
  END COMPONENT IntegerAdder;

  COMPONENT SignificandDivider IS
    PORT (
      x, y : IN STD_LOGIC_VECTOR(e + p - 1 DOWNTO 0);
      significand : OUT STD_LOGIC_VECTOR(2 * p - 2 DOWNTO 0);
      zero : OUT STD_LOGIC;
      shiftVal : OUT INTEGER RANGE 2 * p - 1 DOWNTO 0;
      expIncrement : OUT STD_LOGIC);
  END COMPONENT SignificandDivider;

  COMPONENT ExponentSubstractor IS
    PORT (
      ex, ey : IN STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
      shiftVal : IN INTEGER RANGE 2 * p - 1 DOWNTO 0;
      expIncrement : IN STD_LOGIC;
      ez : OUT STD_LOGIC_VECTOR(e + 1 DOWNTO 0));
  END COMPONENT ExponentSubstractor;

  COMPONENT Denormalizer IS
    PORT (
      significandNorm : IN STD_LOGIC_VECTOR(2 * p - 2 DOWNTO 0) := (OTHERS => '0');
      expNorm : IN STD_LOGIC_VECTOR(e + 1 DOWNTO 0) := (OTHERS => '0');
      significand : OUT STD_LOGIC_VECTOR(p - 2 DOWNTO 0);
      exp : OUT STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
      overflow, subnormal, inexact : OUT STD_LOGIC := '0');
  END COMPONENT Denormalizer;

  SIGNAL sEx, sEy, sEzBeforeShift : STD_LOGIC_VECTOR(e + 1 DOWNTO 0);
  SIGNAL sSignificandN : STD_LOGIC_VECTOR(2 * p - 2 DOWNTO 0);
  SIGNAL sSignificand : STD_LOGIC_VECTOR(p - 2 DOWNTO 0);
  SIGNAL sEzN : STD_LOGIC_VECTOR(e + 1 DOWNTO 0);
  SIGNAL sEz : STD_LOGIC_VECTOR(e - 1 DOWNTO 0);
  SIGNAL sOverflow, sInexact, sSubnormal, sNaN, sInf, sZeroSig, sZero : STD_LOGIC;
  SIGNAL sXNaN, sYNaN, sXInf, sYInf, sXZero, sYZero : STD_LOGIC;
  SIGNAL sShiftVal : INTEGER RANGE 2 * p - 1 DOWNTO 0;
  SIGNAL sExpInc : STD_LOGIC;
BEGIN

  SIGNIFICAND : SignificandDivider PORT MAP(
    x => x, y => y, significand => sSignificandN,
    zero => sZeroSig, shiftVal => sShiftVal, expIncrement => sExpInc);

  EXPONENT : ExponentSubstractor PORT MAP(ex => x(p + e - 2 DOWNTO p - 1), ey => y(p + e - 2 DOWNTO p - 1), shiftVal => sShiftVal, expIncrement => sExpInc, ez => sEzN);

  DENORM : Denormalizer PORT MAP(
    significandNorm => sSignificandN, expNorm => sEzN, significand => sSignificand,
    exp => sEz, subnormal => sSubnormal, inexact => sInexact, overflow => sOverflow);

  --NAN INF CASES
  sXNaN <= '1' WHEN and_reduce(x(e + p - 2 DOWNTO p - 1)) = '1' AND or_reduce(x(p - 2 DOWNTO 0)) = '1' ELSE
    '0';
  sYNaN <= '1' WHEN and_reduce(y(e + p - 2 DOWNTO p - 1)) = '1' AND or_reduce(y(p - 2 DOWNTO 0)) = '1' ELSE
    '0';
  sXInf <= '1' WHEN and_reduce(x(e + p - 2 DOWNTO p - 1)) = '1' AND or_reduce(x(p - 2 DOWNTO 0)) = '0' ELSE
    '0';
  sYInf <= '1' WHEN and_reduce(y(e + p - 2 DOWNTO p - 1)) = '1' AND or_reduce(y(p - 2 DOWNTO 0)) = '0' ELSE
    '0';
  sXZero <= '1' WHEN or_reduce(x(e + p - 2 DOWNTO 0)) = '0' ELSE
    '0';
  sYZero <= '1' WHEN or_reduce(y(e + p - 2 DOWNTO 0)) = '0' ELSE
    '0';

  sNaN <= sXNaN OR sYNaN OR (sYZero AND sXZero) OR (sYInf AND sXInf);
  sInf <= (sXInf OR sYZero) AND NOT sNaN;
  sZero <= szeroSig OR (sYInf AND NOT sNaN AND NOT sInf);
  --SIGN
  z(p + e - 1) <= '0' WHEN sNaN = '1' ELSE
  x(p + e - 1) XOR y(p + e - 1);
  ROUNDN : IF (rMode = toNearest) GENERATE
    --SIGNIFICAND
    z(p - 2 DOWNTO 0) <= (0 => '1', OTHERS => '0') WHEN sNaN = '1' ELSE
    (OTHERS => '0') WHEN sInf = '1' ELSE
    (OTHERS => '0') WHEN sOverflow = '1' ELSE
    (OTHERS => '0') WHEN sZero = '1' ELSE
    sSignificand;

    --EXPONENT
    z(p + e - 2 DOWNTO p - 1) <= (OTHERS => '1') WHEN sNaN = '1' OR sInf = '1' ELSE
    (OTHERS => '1') WHEN sOverflow = '1' ELSE
    (OTHERS => '0') WHEN sZero = '1' OR sSubnormal = '1' ELSE
    sEz;

  END GENERATE;

  ROUNDZ : IF (rMode = toZero) GENERATE
    --SIGNIFICAND
    z(p - 2 DOWNTO 0) <= (0 => '1', OTHERS => '0') WHEN sNaN = '1' ELSE
    (OTHERS => '0') WHEN sInf = '1' ELSE
    (OTHERS => '1') WHEN sOverflow = '1' ELSE
    (OTHERS => '0') WHEN sZero = '1' ELSE
    sSignificand;

    --EXPONENT
    z(p + e - 2 DOWNTO p - 1) <= (OTHERS => '1') WHEN sNaN = '1' OR sInf = '1' ELSE
    (p - 1 => '0', OTHERS => '1') WHEN sOverflow = '1' ELSE
    (OTHERS => '0') WHEN sZero = '1' OR sSubnormal = '1' ELSE
    sEz;
  END GENERATE;

  --FLAGS
  overflow <= sOverflow AND NOT sNaN AND NOT sInf;
  inexact <= sInexact AND NOT sNaN AND NOT sInf;
  zero <= sZero;
  underflow <= sInexact AND sSubnormal;
END structural;