TESTBENCH_DIR=testbench
COMMON_DIR=common
MULTIPLIER_DIR=multiplier
DIVIDER_DIR=divider
ADDER_DIR=adder
OUTPUT=simulation_output
POST_SYNTH_DIR=post_synthesis


all: analyze run

ps: ps-analyze analyze-testbench run

analyze: analyze-multiplier analyze-divider analyze-adder analyze-testbench
	

run: run-testbench-adder

testbench-data-multiplier:
	g++ -std=gnu++11 $(TESTBENCH_DIR)/testbench-multiplier.c -o $(TESTBENCH_DIR)/testbench -lm
	$(TESTBENCH_DIR)/testbench

testbench-data-divider:
	g++ -std=gnu++11 $(TESTBENCH_DIR)/testbench-divider.c -o $(TESTBENCH_DIR)/testbench -lm
	$(TESTBENCH_DIR)/testbench

testbench-data-adder:
	g++ -std=gnu++11 $(TESTBENCH_DIR)/testbench-adder.c -o $(TESTBENCH_DIR)/testbench -lm
	$(TESTBENCH_DIR)/testbench

testbench-data-10b-multiplier:
	g++ -std=gnu++11 $(TESTBENCH_DIR)/testbench-multiplier_10bits.cpp $(TESTBENCH_DIR)/float10.cpp -o $(TESTBENCH_DIR)/testbench
	$(TESTBENCH_DIR)/testbench

testbench-data-10b-divider:
	g++ -std=gnu++11 $(TESTBENCH_DIR)/testbench-divider_10bits.cpp $(TESTBENCH_DIR)/float10.cpp -o $(TESTBENCH_DIR)/testbench
	$(TESTBENCH_DIR)/testbench

testbench-data-10b-adder:
	g++ -std=gnu++11 $(TESTBENCH_DIR)/testbench-adder_10bits.cpp $(TESTBENCH_DIR)/float10.cpp -o $(TESTBENCH_DIR)/testbench
	$(TESTBENCH_DIR)/testbench

analyze-common:
	ghdl -a --ieee=synopsys $(COMMON_DIR)/HalfAdder.vhdl $(COMMON_DIR)/FullAdder.vhdl $(COMMON_DIR)/IntegerAdder.vhdl $(COMMON_DIR)/FPUTypes.vhdl $(COMMON_DIR)/PartialProducts.vhdl $(COMMON_DIR)/IntegerDivider.vhdl $(COMMON_DIR)/IntegerDividerSRT2.vhdl $(COMMON_DIR)/IntegerDividerSRT4_MAXRED.vhdl $(COMMON_DIR)/IntegerDividerSRT4_MINRED.vhdl $(COMMON_DIR)/IntegerMultiplier.vhdl $(COMMON_DIR)/IntegerMultiplierMul.vhdl $(COMMON_DIR)/dadda_multiplier/half_adder.vhd $(COMMON_DIR)/dadda_multiplier/full_adder.vhd $(COMMON_DIR)/dadda_multiplier/mbe_ppg.vhd $(COMMON_DIR)/dadda_multiplier/Mult_Dadda24.vhd $(COMMON_DIR)/SignificandRounding.vhdl $(COMMON_DIR)/LeadingZerosDetector.vhdl $(COMMON_DIR)/LeadingZerosShifter.vhdl $(COMMON_DIR)/Denormalizer.vhdl 

analyze-multiplier: analyze-common
	ghdl -a --ieee=synopsys $(MULTIPLIER_DIR)/SignificandMultiplier.vhdl $(MULTIPLIER_DIR)/ExponentAdder.vhdl $(MULTIPLIER_DIR)/FloatMultiplier.vhdl

analyze-divider: analyze-common
	ghdl -a --ieee=synopsys $(DIVIDER_DIR)/SignificandDivider.vhdl $(DIVIDER_DIR)/ExponentSubstractor.vhdl $(DIVIDER_DIR)/FloatDivider.vhdl

analyze-adder: analyze-common
	ghdl -a --ieee=synopsys $(ADDER_DIR)/FloatAdder.vhdl $(ADDER_DIR)/OperandSwitch.vhdl $(ADDER_DIR)/SignificandAdder.vhdl $(ADDER_DIR)/SignificandSubstractor.vhdl $(ADDER_DIR)/FloatAdderRounding.vhdl

analyze-testbench:
	ghdl -a --ieee=synopsys $(TESTBENCH_DIR)/sim_FloatMultiplier.vhdl $(TESTBENCH_DIR)/sim_FloatAdder.vhdl $(TESTBENCH_DIR)/sim_FloatDivider.vhdl

run-testbench-10b-multiplier: analyze testbench-data-10b-multiplier
	mkdir -p $(OUTPUT)
	cd $(OUTPUT) && ghdl -r --ieee=synopsys --workdir=.. test_floatm --stop-time=20000000ns

run-testbench-multiplier: analyze testbench-data-multiplier
	mkdir -p $(OUTPUT)
	cd $(OUTPUT) && ghdl -r --ieee=synopsys --workdir=.. test_floatm --stop-time=20000000ns --vcd=floatm.vcd

run-testbench-10b-divider: analyze testbench-data-10b-divider
	mkdir -p $(OUTPUT)
	cd $(OUTPUT) && ghdl -r --ieee=synopsys --workdir=.. test_floatd --stop-time=20000000ns

run-testbench-divider: analyze testbench-data-divider
	mkdir -p $(OUTPUT)
	cd $(OUTPUT) && ghdl -r --ieee=synopsys --workdir=.. test_floatd --stop-time=20000000ns --vcd=floatd.vcd

run-testbench-10b-adder: analyze testbench-data-10b-adder
	mkdir -p $(OUTPUT)
	cd $(OUTPUT) && ghdl -r --ieee=synopsys --workdir=.. test_floata --stop-time=20000000ns

run-testbench-adder: analyze testbench-data-adder
	mkdir -p $(OUTPUT)
	cd $(OUTPUT) && ghdl -r --ieee=synopsys --workdir=.. test_floata --stop-time=20000000ns --vcd=floata.vcd

clean:
	rm -f *.o synthesis/*.o simulation_output/* work-obj93.cf synthesis/work-obj93.cf