// Copyright 2022 Inria and University of Rennes.
// Copyright and related rights are licensed under the Solderpad Hardware
// License, Version 2.1 (the "License"); you may not use this file except in
// compliance with the License.  You may obtain a copy of the License at
// http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
// or agreed to in writing, software, hardware and materials distributed under
// this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

////////////////////////////////////////////////////////////////////////////////
// Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   //
//                     Olivier Sentieys - olivier.sentieys@inria.fr           //
//                     Silviu Filip - silviu.filip@inria.fr                   //
// Project Name:   FPU                                                        //
// Language:       C++                                                        //
//                                                                            //
// Description:    part of the testbench generator                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <float.h>
#include <math.h>
#include <fenv.h>

#define S_MSK 0x80000000
#define E_MSK 0x7F800000
#define M_MSK 0x007FFFFF
#define NAN_FLOAT 0x7F800001;

#define NBR_RANDOM_TESTS 10000

#pragma STDC FENV_ACCESS ON

void printFloatBinary(float f, FILE* dest){
  u_int32_t *uiPtr = (u_int32_t*)&f;

  for(int i=31;i>=0;i--){
    if((*uiPtr>>i) & 1){
      fprintf(dest,"1");
    }else{
      fprintf(dest,"0");
    }
  }
}

void printFloatFields(float f){
  u_int32_t *uiPtr = (u_int32_t*)&f;

  printf("float %f (%X) : S=%X , E=%X , M=%X\n", f, *uiPtr, (*uiPtr & S_MSK)>>31, (*uiPtr & E_MSK)>>23, (*uiPtr & M_MSK));
}

float randomFloat(){

  
  
  int negative = ((float)rand()/RAND_MAX)>0.5?1:0;
  float r = ((float)rand()/RAND_MAX);
  //printf("%f\n", r);
  int subnormal = 0;
  int nan = 0;
  int inf = 0;
  int zero = 0;
  if(r<0.2){
    subnormal=1;
  }else if(r<0.3){
    nan = 1;
  }else if(r<0.4){
    inf = 1;
  }else if(r<0.5){
    zero = 1;
  }
  //printf("sub :%d\n", subnormal);
  u_int32_t val = 0;

  if(negative){
    val |= 0x0001;
  }

  unsigned char exp;

  if(subnormal || zero){
    exp = 0;
  }else if(nan || inf){
    exp = 255;
  }else{
    exp = 1.0+(((float)rand()/RAND_MAX)*254.0);
    //exp = 127.0+(((float)rand()/RAND_MAX)*20.0);
    //printf("exp: %d\n",exp);
  }
  val = val<<8;
  val |= exp;

  

  u_int32_t m;

  if(zero || inf){
    m = 0;
  }else if(nan){
    m = 1;
  }else{
    m = (u_int32_t)(((float)rand()/RAND_MAX)*pow(2, 24));
  }
   

  val = val<<23;
  val |= m;


  //printf("%d\n",val);
  float *f = (float*)&val;

  return *f;
}

float nanValue(){
  u_int32_t val = NAN_FLOAT;
  float *f = (float*)&val;
  return *f;
}


int main(){
  //time_t t;
  //srand((unsigned) time(&t));
  
  //fesetround(FE_TOWARDZERO); //uncomment to round to zero
  
  FILE * fptr;
  fptr = fopen("testbench/test_vectors.txt","w");

  for(int i=0;i<NBR_RANDOM_TESTS;i++){
    float f1 = randomFloat();
    float f2 = randomFloat();
    float f3 = f1+f2;
    f3 = isnan(f3)?nanValue():f3;
    //printf("%f\n",f1);
    printFloatBinary(f1, fptr);
    fprintf(fptr," ");
    printFloatBinary(f2, fptr);
    fprintf(fptr," ");
    printFloatBinary(f3, fptr);
    fprintf(fptr,"\n");
  }
  
  fclose(fptr);
}