-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    Test_FLOATM                                                --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    tesbench of FloatMultiplier                                --
--                                                                            --
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use STD.textio.all;
use ieee.std_logic_textio.all;

library work;
use work.fputypes.all;

entity Test_FLOATM is 
end Test_FLOATM;

architecture ghdl_sim of Test_FLOATM  is
   component FloatMultiplier is
      port (x, y: in std_logic_vector(e+p-1 downto 0):=(others=>'0');
         z: out std_logic_vector(e+p-1 downto 0):=(others=>'0');
         overflow, underflow, inexact, zero: out std_logic:='0');
   end component FloatMultiplier;

   signal SX, SY, SZ: std_logic_vector(e+p-1 downto 0) := (others=>'0');
   signal sim_ok: std_logic := '1';

   constant cycle: time := 10 ns;

   file fileTestVectors: text;

begin

   -- Component Instantiation
   U1: FloatMultiplier port map(x=>SX, y=>SY, z=>SZ);	

   Simulation: process
      variable vLine: line;
      variable vOperand1, vOperand2, vResult: std_logic_vector(e+p-1 downto 0);
      variable vSpaceChar: character;
      variable i: integer := 0;
   begin
      sim_ok <= '1';
      file_open(fileTestVectors, "../testbench/test_vectors.txt", read_mode);
      while not endfile(fileTestVectors) loop
         readline(fileTestVectors, vLine);
         read(vLine, vOperand1);
         read(vLine, vSpaceChar);
         read(vLine, vOperand2);
         read(vLine, vSpaceChar);
         read(vLine, vResult);
         SX <= vOperand1;
         SY <= vOperand2;
         wait for cycle;

         assert (SZ(e+p-1) = vResult(e+p-1)) 
	      report "Problem on sign"
	      severity warning;
         assert (SZ(e+p-2 downto p-1) = vResult(e+p-2 downto p-1)) 
	      report "Problem on exponent, expected: "&integer'image(to_integer(unsigned((vResult(e+p-2 downto p-1)))))&", found: "&integer'image(to_integer(unsigned((SZ(e+p-2 downto p-1)))))
	      severity warning;
	      assert (SZ(p-2 downto 0) = vResult(p-2 downto 0)) 
	      report "Problem on significand, expected: "&integer'image(to_integer(unsigned((vResult(p-2 downto 0)))))&", found: "&integer'image(to_integer(unsigned((SZ(p-2 downto 0)))))
	      severity warning;
         if (SZ(e+p-1) /= vResult(e+p-1)) OR (SZ(e+p-2 downto p-1) /= vResult(e+p-2 downto p-1)) OR (SZ(p-2 downto 0) /= vResult(p-2 downto 0)) then
            sim_ok <= '0';
         end if;
         i := i + 1;
      end loop;

      file_close(fileTestVectors);

      report "end of simulation";
      if sim_ok = '1' then
         report "ALL " & integer'image(i) & " TESTS PASSED";
      else
         report "PROBLEM DURING SIMULATION";
      end if;

      wait;

   end process;
end ghdl_sim;

-- architecture postsynth_sim of Test_FLOATM  is
--    component 	FloatMultiplier IS
--     PORT (
-- 	x : IN std_logic_vector(31 DOWNTO 0);
-- 	y : IN std_logic_vector(31 DOWNTO 0);
-- 	z : BUFFER std_logic_vector(31 DOWNTO 0);
-- 	overflow : BUFFER std_logic;
-- 	underflow : BUFFER std_logic;
-- 	inexact : BUFFER std_logic;
-- 	zero : BUFFER std_logic
-- 	);
--    END component FloatMultiplier;

--    signal SX, SY, SZ: std_logic_vector(31 downto 0) := (others=>'0');

--    constant cycle: time := 10 ns;

--    file fileTestVectors: text;

-- begin

--    -- Component Instantiation
--    U1: FloatMultiplier port map(x=>SX, y=>SY, z=>SZ);	

--    Simulation: process
--       variable vLine: line;
--       variable vOperand1, vOperand2, vResult: std_logic_vector(31 downto 0);
--       variable vSpaceChar: character;
--    begin
--       file_open(fileTestVectors, "testbench/test_vectors.txt", read_mode);
--       while not endfile(fileTestVectors) loop
--          readline(fileTestVectors, vLine);
--          read(vLine, vOperand1);
--          read(vLine, vSpaceChar);
--          read(vLine, vOperand2);
--          read(vLine, vSpaceChar);
--          read(vLine, vResult);
--          SX <= vOperand1;
--          SY <= vOperand2;
--          wait for cycle;

--          assert (SZ(31) = vResult(31)) 
-- 	      report "Problem on sign"
-- 	      severity warning;
--          assert (SZ(30 downto 23) = vResult(30 downto 23)) 
-- 	      report "Problem on exponent, expected: "&integer'image(to_integer(unsigned((vResult(30 downto 23)))))&", found: "&integer'image(to_integer(unsigned((SZ(30 downto 23)))))
-- 	      severity warning;
-- 	      assert (SZ(22 downto 0) = vResult(22 downto 0)) 
-- 	      report "Problem on significand, expected: "&integer'image(to_integer(unsigned((vResult(22 downto 0)))))&", found: "&integer'image(to_integer(unsigned((SZ(22 downto 0)))))
-- 	      severity warning;
--       end loop;

--       file_close(fileTestVectors);

-- 	   wait;

--    end process;
-- end postsynth_sim;
