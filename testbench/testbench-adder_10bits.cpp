// Copyright 2022 Inria and University of Rennes.
// Copyright and related rights are licensed under the Solderpad Hardware
// License, Version 2.1 (the "License"); you may not use this file except in
// compliance with the License.  You may obtain a copy of the License at
// http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
// or agreed to in writing, software, hardware and materials distributed under
// this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

////////////////////////////////////////////////////////////////////////////////
// Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   //
//                     Olivier Sentieys - olivier.sentieys@inria.fr           //
//                     Silviu Filip - silviu.filip@inria.fr                   //
// Project Name:   FPU                                                        //
// Language:       C++                                                        //
//                                                                            //
// Description:    part of the testbench generator                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <math.h>
#include "float10.hpp"

using namespace std;

void printFloat16Binary(float10 x, FILE *dest)
{
  u_int16_t *xui = (u_int16_t *)&x;
  for (int i = 9; i >= 0; i--)
  {
    if (((*xui) >> i) & 1)
    {
      fprintf(dest, "1");
    }
    else
    {
      fprintf(dest, "0");
    }
  }
}

int main(){
  FILE *fptr;
  fptr = fopen("testbench/test_vectors.txt", "w");

  float10 x;
  u_int16_t *xui = (u_int16_t *)&x;
  float10 y;
  u_int16_t *yui = (u_int16_t *)&y;

  int k=1;

  for (int i = 0; i < 1024; i++){
    for (int j = 0; j < 1024; j++){
      if(1){
      *xui = (u_int16_t)i;
      *yui = (u_int16_t)j;

      float10 z = x + y;
      
        //cout<<x<<" "<<y<<" "<<z<<endl;
      
      
      printFloat16Binary(x, fptr);
      fprintf(fptr, " ");
      printFloat16Binary(y, fptr);
      fprintf(fptr, " ");
      printFloat16Binary(z, fptr);
      fprintf(fptr, "\n");

      
      }
      k++;
    }
  }

  fclose(fptr);
  return 0;
}
