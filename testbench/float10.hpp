// Copyright 2022 Inria and University of Rennes.
// Copyright and related rights are licensed under the Solderpad Hardware
// License, Version 2.1 (the "License"); you may not use this file except in
// compliance with the License.  You may obtain a copy of the License at
// http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
// or agreed to in writing, software, hardware and materials distributed under
// this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

////////////////////////////////////////////////////////////////////////////////
// Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   //
//                     Olivier Sentieys - olivier.sentieys@inria.fr           //
//                     Silviu Filip - silviu.filip@inria.fr                   //
// Project Name:   FPU                                                        //
// Language:       C++                                                        //
//                                                                            //
// Description:    part of the testbench generator                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////


#ifndef HALF_PRECISION_LIBRARY_H
#define HALF_PRECISION_LIBRARY_H

#pragma once

#include <cstdint>
#include <functional>

struct float10 {
  float10() = default;

  float10(const float10&) = default;

  float10(float10&&) = default;

  float10(const float& rhs);

  float10& operator=(const float& rhs);

  // Operator +=, -=, *=, /=
#define BINARY_ARITHMETIC_OPERATOR(OP)                                         \
    float10& operator OP##=(const float10& rhs) {                              \
        *this = operator float() OP static_cast<float>(rhs);                   \
        return *this;                                                          \
    }

  BINARY_ARITHMETIC_OPERATOR(+)

  BINARY_ARITHMETIC_OPERATOR(-)

  BINARY_ARITHMETIC_OPERATOR(*)

  BINARY_ARITHMETIC_OPERATOR(/)

#undef BINARY_ARITHMETIC_OPERATOR

  // Operator ++, --
  float10& operator++() {
    *this += 1;
    return *this;
  }

  float10 operator++(int) {
    float10 ret(*this);
    operator++();
    return ret;
  }

  float10& operator--() {
    *this -= 1;
    return *this;
  }

  float10 operator--(int) {
    float10 ret(*this);
    operator--();
    return ret;
  }

  // Operator float
  operator float() const;

  template<typename Key> friend
  struct std::hash;

private:
  uint16_t buf;
};

namespace std {

  template<>
  struct hash<float10> {
    std::size_t operator()(const float10& key) const {
      return hash<uint16_t>()(key.buf);
    }
  };

}

#endif
