-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    Test_INTA                                                  --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    tesbench of IntegerAdder                                   --
--                                                                            --
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity Test_INTA is 
end Test_INTA;

architecture test of Test_INTA  is

   component IntegerAdder
      generic (n: positive);
      port (a, b: in std_logic_vector(n-1 downto 0);
         c_in: in std_logic;
         sum: out std_logic_vector(n-1 downto 0);
         carry: out std_logic);
   end component;

   signal SA, SB, SSum: std_logic_vector(7 downto 0) := (others=>'0');
   signal SCin , SCout: std_logic := '0';

   constant cycle: time := 10 ns;

   -- configuration
   for U1:IntegerAdder use entity work.IntegerAdder(structural);

begin

   -- Component Instantiation
   U1: IntegerAdder generic map(n=>8)
      port map(a=>SA, b=>SB, sum=>SSum, c_in=>SCin, carry=>SCout);	

   Simulation: process 
   begin
      SA <= "00000001"; 
      SB <= "00000001";
      SCin <= '0';
      wait for cycle;
      assert (SCout = '0') 
	   report "Problem on carry"
	   severity warning;
	   assert (SSum = "00000010") 
	   report "Problem on sum" 
	   severity warning;
      
      SA <= "00000001"; 
      SB <= "11111111";
      SCin <= '0';
      wait for cycle;
      assert (SCout = '0') 
	   report "Problem on carry"
	   severity warning;
	   assert (SSum = "00000000") 
	   report "Problem on sum" 
	   severity warning;

      SA <= "11111111"; 
      SB <= "11111111";
      SCin <= '0';
      wait for cycle;
      assert (SCout = '0') 
	   report "Problem on carry"
	   severity warning;
	   assert (SSum = "11111110") 
	   report "Problem on sum" 
	   severity warning;

      SA <= "00000001"; 
      SB <= "00000001";
      SCin <= '1';
      wait for cycle;
      assert (SCout = '0') 
	   report "Problem on carry"
	   severity warning;
	   assert (SSum = "00000011") 
	   report "Problem on sum" 
	   severity warning;

      SA <= "00000001"; 
      SB <= "11111111";
      SCin <= '1';
      wait for cycle;
      assert (SCout = '0') 
	   report "Problem on carry"
	   severity warning;
	   assert (SSum = "00000001") 
	   report "Problem on sum" 
	   severity warning;

      SA <= "11111111"; 
      SB <= "11111111";
      SCin <= '1';
      wait for cycle;
      assert (SCout = '0') 
	   report "Problem on carry"
	   severity warning;
	   assert (SSum = "11111111") 
	   report "Problem on sum" 
	   severity warning;

      SA <= "01111111"; 
      SB <= "00000001";
      SCin <= '0';
      wait for cycle;
      assert (SCout = '1') 
	   report "Problem on carry"
	   severity warning;
	   assert (SSum = "10000000") 
	   report "Problem on sum" 
	   severity warning;

      SA <= "10000000"; 
      SB <= "11111111";
      SCin <= '0';
      wait for cycle;
      assert (SCout = '1') 
	   report "Problem on carry"
	   severity warning;
	   assert (SSum = "01111111") 
	   report "Problem on sum" 
	   severity warning;

      SA <= "01111111"; 
      SB <= "00000001";
      SCin <= '1';
      wait for cycle;
      assert (SCout = '1') 
	   report "Problem on carry"
	   severity warning;
	   assert (SSum = "10000001") 
	   report "Problem on sum" 
	   severity warning;

      SA <= "10000000"; 
      SB <= "11111111";
      SCin <= '1';
      wait for cycle;
      assert (SCout = '0') 
	   report "Problem on carry"
	   severity warning;
	   assert (SSum = "10000000") 
	   report "Problem on sum" 
	   severity warning;

	   wait;

   end process;
end test;

