// Copyright 2022 Inria and University of Rennes.
// Copyright and related rights are licensed under the Solderpad Hardware
// License, Version 2.1 (the "License"); you may not use this file except in
// compliance with the License.  You may obtain a copy of the License at
// http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
// or agreed to in writing, software, hardware and materials distributed under
// this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

////////////////////////////////////////////////////////////////////////////////
// Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   //
//                     Olivier Sentieys - olivier.sentieys@inria.fr           //
//                     Silviu Filip - silviu.filip@inria.fr                   //
// Project Name:   FPU                                                        //
// Language:       C++                                                        //
//                                                                            //
// Description:    part of the testbench generator                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#ifndef HALF_PRECISION_LIBRARY_H
#define HALF_PRECISION_LIBRARY_H

#pragma once

#include <cstdint>
#include <functional>

#define HLF_EPSILON     1e-3
#define HLF_MAX         65504
#define HLF_MIN         6.1035e-05

struct float16 {
  float16() = default;

  float16(const float16&) = default;

  float16(float16&&) = default;

  float16(const float& rhs);

  float16& operator=(const float& rhs);

  // Operator +=, -=, *=, /=
#define BINARY_ARITHMETIC_OPERATOR(OP)                                         \
    float16& operator OP##=(const float16& rhs) {                              \
        *this = operator float() OP static_cast<float>(rhs);                   \
        return *this;                                                          \
    }

  BINARY_ARITHMETIC_OPERATOR(+)

  BINARY_ARITHMETIC_OPERATOR(-)

  BINARY_ARITHMETIC_OPERATOR(*)

  BINARY_ARITHMETIC_OPERATOR(/)

#undef BINARY_ARITHMETIC_OPERATOR

  // Operator ++, --
  float16& operator++() {
    *this += 1;
    return *this;
  }

  float16 operator++(int) {
    float16 ret(*this);
    operator++();
    return ret;
  }

  float16& operator--() {
    *this -= 1;
    return *this;
  }

  float16 operator--(int) {
    float16 ret(*this);
    operator--();
    return ret;
  }

  // Operator float
  operator float() const;

  template<typename Key> friend
  struct std::hash;

private:
  uint16_t buf;
};

namespace std {

  template<>
  struct hash<float16> {
    std::size_t operator()(const float16& key) const {
      return hash<uint16_t>()(key.buf);
    }
  };

}

#endif
