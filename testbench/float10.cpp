// Copyright 2022 Inria and University of Rennes.
// Copyright and related rights are licensed under the Solderpad Hardware
// License, Version 2.1 (the "License"); you may not use this file except in
// compliance with the License.  You may obtain a copy of the License at
// http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
// or agreed to in writing, software, hardware and materials distributed under
// this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

////////////////////////////////////////////////////////////////////////////////
// Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   //
//                     Olivier Sentieys - olivier.sentieys@inria.fr           //
//                     Silviu Filip - silviu.filip@inria.fr                   //
// Project Name:   FPU                                                        //
// Language:       C++                                                        //
//                                                                            //
// Description:    part of the testbench generator                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#include "float10.hpp"
#include <iostream>

#include <emmintrin.h>

static uint16_t float2half(const float& val) {
#ifndef __F16CINTRIN_H
  const uint32_t bits = *reinterpret_cast<const uint32_t*>(&val);

  // Extract the sign from the float value
  uint16_t sign = (bits & 0x80000000) >> 22;
  // Extract the fraction from the float value
  const uint32_t frac32 = bits & 0x7fffff;
  // Extract the exponent from the float value
  const uint8_t exp32 = (bits & 0x7f800000) >> 23;
  const int16_t exp32_diff = (int16_t)exp32 - 127;

  uint16_t exp10 = 0;
  uint16_t frac10 = frac32 >> 18;

  if (__builtin_expect(exp32_diff > 7, 0)) {
    exp10 = 0xf;
  } else if (__builtin_expect(exp32_diff < -6, 0)) {
    exp10 = 0;
  } else {
    exp10 = exp32_diff + 7;
  }

  //std::cout<<exp32_diff<<std::endl;

  if (__builtin_expect(exp32 == 0xff && frac32 != 0, 0)) {// 0 && frac16 == 0, 0)) {
    // corner case 1: NaN
    // This case happens when FP32 value is NaN whose the fraction part
    // transformed to FP16 counterpart is truncated to 0. We need to flip the
    // high bit to 1 to make it distinguished from inf.
    frac10 = 0x001;
    sign = 0;
  } else if (__builtin_expect(exp32 == 0 || (exp32 == 0xff && frac32 == 0), 0)) {
    // corner case 2: subnormal
    // All FP32 subnormal values are under the range of FP16 so the fraction
    // part is set to 0.
    // corner case 3: infinite
    frac10 = 0;

  } else if (__builtin_expect(exp10 == 0xf && exp32 != 0xff, 0)) {
    // corner case 4: overflow
    exp10 = 0xe;
    frac10 = 0x1f;
  } else if (__builtin_expect(exp10 == 0 && exp32 != 0, 0)) {
    // corner case 5: underflow
    // We use `truncate` mode here.
    int shift = std::min(-7-exp32_diff, 6);
    frac10 = (0x10 | (frac10 >> 1)) >> shift;
    //std::cout<<(-13-exp32_diff)<<" "<<shift<<" "<<frac16<<" "<<(0x200 | (frac16 >> 1))<<std::endl;
  }

  // Compose the final FP16 binary
  uint16_t ret = 0;
  ret |= sign;
  ret |= exp10 << 5;
  ret |= frac10;

  return ret;
#else
  return _cvtss_sh(val, 0);
#endif
}

static float half2float(const uint16_t& val) {
#ifndef __F16CINTRIN_H
  // Extract the sign from the bits
  const uint32_t sign = static_cast<uint32_t>(val & 0x200) << 22;
  // Extract the exponent from the bits
  const uint16_t exp10 = (val & 0x1e0) >> 5;
  // Extract the fraction from the bits
  uint16_t frac10 = val & 0x1f;

  uint32_t exp32 = 0;
  if (__builtin_expect(exp10 == 0xf, 0)) {
    exp32 = 0xff;
  } else if (__builtin_expect(exp10 == 0, 0)) {
    exp32 = 0;
  } else {
    exp32 = static_cast<uint32_t>(exp10) + 120;
  }

  // corner case: subnormal -> normal
  // The denormal number of FP16 can be represented by FP32, therefore we need
  // to recover the exponent and recalculate the fration.
  if (__builtin_expect(exp10 == 0 && frac10 != 0, 0)) {
    uint8_t OffSet = 0;
    do {
      ++OffSet;
      frac10 <<= 1;
    } while ((frac10 & 0x20) != 0x20);
    // mask the 9th bit
    frac10 &= 0x1f;
    exp32 = 121 - OffSet;
  }

  uint32_t frac32 = frac10 << 18;

  // Compose the final FP32 binary
  uint32_t bits = 0;

  bits |= sign;
  bits |= (exp32 << 23);
  bits |= frac32;

  return *reinterpret_cast<float*>(&bits);
#else
  return _cvtsh_ss(val);
#endif
}

std::ostream& operator<<(std::ostream& os, const float10& val) {
  os << static_cast<float>(val);
  return os;
}

std::istream& operator>>(std::istream& is, float10& val_h) {
  float val_f = 0.0f;
  is >> val_f;
  val_h = val_f;
  return is;
}

float10::float10(const float& rhs) : buf(float2half(rhs)) {}

float10::operator float() const {
  return half2float(buf);
}

float10& float10::operator=(const float& rhs) {
  buf = float2half(rhs);
  return *this;
}

// Operator +, -, *, /
#define BINARY_ARITHMETIC_OPERATOR(OP)                                         \
float10 operator OP(float10 lhs, const float10& rhs) {                         \
    lhs OP##= rhs;                                                             \
    return lhs;                                                                \
}

BINARY_ARITHMETIC_OPERATOR(+)

BINARY_ARITHMETIC_OPERATOR(-)

BINARY_ARITHMETIC_OPERATOR(*)

BINARY_ARITHMETIC_OPERATOR(/)

#undef BINARY_ARITHMETIC_OPERATOR

// Operator <, >, <=, >=
bool operator<(const float10& lhs, const float10& rhs) {
  return static_cast<float>(lhs) < static_cast<float>(rhs);
}

bool operator>(const float10& lhs, const float10& rhs) {
  return rhs < lhs;
}

bool operator<=(const float10& lhs, const float10& rhs) {
  return !(lhs > rhs);
}

bool operator>=(const float10& lhs, const float10& rhs) {
  return !(lhs < rhs);
}

// Operator ==, !=
bool operator==(const float10& lhs, const float10& rhs) {
  return static_cast<float>(lhs) == static_cast<float>(rhs);
}

bool operator!=(const float10& lhs, const float10& rhs) {
  return !(lhs == rhs);
}
