-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    Test_INTM                                                  --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    tesbench of IntegerMultiplier                              --
--                                                                            --
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

library work;
use work.fputypes.all;

entity Test_INTM is 
end Test_INTM;

architecture test of Test_INTM  is

   component IntegerMultiplier is
      port (a, b: in std_logic_vector(NbitMul-1 downto 0);
         result: out std_logic_vector(NbitMul*2-1 downto 0));
   end component;

   signal SA, SB: std_logic_vector(NbitMul-1 downto 0) := (others=>'0');
   signal SProduct: std_logic_vector(NbitMul*2-1 downto 0) := (others=>'0');

   constant cycle: time := 10 ns;

   -- configuration
   for U1:IntegerMultiplier use entity work.IntegerMultiplier(structural);

begin

   -- Component Instantiation
   U1: IntegerMultiplier port map(a=>SA, b=>SB, result=>SProduct);	

   Simulation: process 
   begin
      SA <= x"000001";
      SB <= x"000001";
      wait for cycle;
	   assert (SProduct = x"000000000001")
	   report "Problem on product" 
	   severity warning;

      SA <= x"00000A";
      SB <= x"000011";
      wait for cycle;
	   assert (SProduct = x"0000000000AA")
	   report "Problem on product" 
	   severity warning;

      SA <= x"800001";
      SB <= x"AAAAAA";
      wait for cycle;
	   assert (SProduct = x"555555AAAAAA")
	   report "Problem on product" 
	   severity warning;
	   wait;

   end process;
end test;

