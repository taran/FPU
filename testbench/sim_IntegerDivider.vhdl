-- Copyright 2022 Inria and University of Rennes.
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.1 (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.1. Unless required by applicable law
-- or agreed to in writing, software, hardware and materials distributed under
-- this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------
-- Contributors:       Thomas Mevel - thomas.mevel@inria.fr                   --
--                     Olivier Sentieys - olivier.sentieys@inria.fr           --
--                     Silviu Filip - silviu.filip@inria.fr                   --
-- Design Name:    Test_INTD                                                  --
-- Project Name:   FPU                                                        --
-- Language:       VHDL                                                       --
--                                                                            --
-- Description:    tesbench of IntegerDivider                                 --
--                                                                            --
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity Test_INTD is 
end Test_INTD;

architecture test of Test_INTD  is

   component IntegerDivider is
      generic(n: positive:=1);
	   port (num, den: in std_logic_vector(n-1 downto 0);
         q: out std_logic_vector(n-1 downto 0));
   end component IntegerDivider;

   signal SA, SB, SDivN, SDivSRT: std_logic_vector(7 downto 0);

   constant cycle: time := 10 ns;

   -- configuration
   for U1:IntegerDivider use entity work.IntegerDivider(srt4_minred);
   for U2:IntegerDivider use entity work.IntegerDivider(srt2);

begin

   -- Component Instantiation
   U1: IntegerDivider generic map(n=>8)
      port map(num=>SA, den=>SB, q=>SDivN);

   U2: IntegerDivider generic map(n=>8)
      port map(num=>SA, den=>SB, q=>SDivSRT);

   Simulation: process 
   begin
      SA <= "00010100"; 
      SB <= "11000000";
      wait for cycle;
      

	   wait;

   end process;
end test;

